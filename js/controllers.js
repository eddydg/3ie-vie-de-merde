var app = angular.module("VDMApp", ['ngSanitize']);

app.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                for (var i = 0;i<files.length;i++) {
                    scope.$emit("fileSelected", { file: files[i] });
                }                                       
            });
        }
    };
});

app.run(function($http, $window) {
    $http.defaults.headers.common['Api-Key'] = 'C4qkaoFiQuhgXTRez0jfn5U7DWVvKxI82rdsBbcALJMm9pYlwZE3GNH6Sy1OPt';

    getUser = function() {
        if (!$window.sessionStorage.length)
            return null;
        var user;
        for (user in $window.sessionStorage) break;
        var str = $window.sessionStorage.getItem(user);
        var val = JSON.parse(str);

        return val;
    }

    updateMenu = function() {
        var user = getUser();
        if (user) {
            $("#login-link").hide();
            $("#signup-link").hide();
            $("#logout-link").show();
            $("#sayHello").html("Bonjour " + user.username);
        }
        else {
            $("#login-link").show();
            $("#signup-link").show();
            $("#logout-link").hide();
            $("#sayHello").html();
        }
    }
    updateMenu();

});

app.controller("menuctrl", function($rootScope, $scope, $window) {
    var user = getUser();
    $scope.logout = function() {
        if (user) {
            toastr.success("Vous avez été déconnecté.");
            $window.sessionStorage.removeItem(user.username);
            updateMenu();

            if (location.pathname == "/login.html") {
                window.setTimeout(function() {
                        window.location.replace("index.html");
                    }, 1500);
            }
            else {
                $rootScope.$broadcast('hidePosts');
            }

        }
        else
            toastr.info("Vous n'êtes pas connecté.");
    }
    
});

app.controller("navctrl", function($rootScope, $scope, $window) {
    var user = getUser();
    $scope.logout = function() {
        if (user) {
            toastr.success("Vous avez été déconnecté.");
            $window.sessionStorage.removeItem(user.username);
            updateMenu();

            if (location.pathname == "/login.html") {
                window.setTimeout(function() {
                        window.location.replace("index.html");
                    }, 1500);
            }
            else {
                $rootScope.$broadcast('hidePosts');
            }

        }
        else
            toastr.info("Vous n'êtes pas connecté.");
    }

});

app.controller("postscontroller", function($rootScope, $scope, $http, $window) {

    user = getUser();
    updateMenu();
    if (!user)
    {
        $("#cta").hide();
        $("#vdm-actions").hide();
        return;
    }
    $scope.loading = true;
    $("#defaultBox").hide();

    var request = $http({
        method: "GET",
        url: "http://vdm.3ie.fr/status/feed",
        headers: {
            'Authorization': user.token
        }
    });
    
    request.success(function(data) {
        $scope.posts = data;
        $scope.loading = false;
    });

    request.error(function(data) {
        toastr.error('Une erreur est survenue. Veuillez réessayer plus tard.');
        console.log(JSON.stringify(data));
        $scope.loading = false;
    });


    $scope.$on('hidePosts', function(event) {
        $("#cta").fadeOut();
        $("#vdm-actions").fadeOut();
        $("#posts-vdm").fadeOut();
        $("#defaultBox").fadeIn();
    });

    $scope.$on('updatePosts', function(event) {
        var request = $http({
            method: "GET",
            url: "http://vdm.3ie.fr/status/feed",
            headers: {
                'Authorization': user.token
            }
        });
        
        request.success(function(data) {
            $scope.posts = data;
        });
    });

    $scope.vdmConfirm = function(post, confirm) {
        var user = getUser();
        var request = $http({
            method: "POST",
            url: "http://vdm.3ie.fr/status/" + post.id + "/confirm",
            data: {
                confirmation: confirm
            },
            headers: {
                "Authorization": user.token
            }
        });
        
        request.success(function(data) {
            toastr.success("C'est noté !");
            $rootScope.$broadcast('updatePosts');
        });

        request.error(function(data) {
            if (data.code == 400)
                toastr.warning("Veuillez remplir le formulaire.");
            else if (data.code == 401)
                toastr.error("Informations incorrectes.");
            else if (data.code == 404)
                toastr.error("Cette VDM n'existe pas");
            else if (data.code == 409)
                toastr.warning("Vous avez déjà voté pour cette VDM.");
            else if (data.code == 422)
                toastr.error("Soit vous êtes d'accord, soit en désaccord. Il n'y a pas d'autre choix.");
            else
                toastr.error("Une erreur de connexion est survenue.\nVeuillez réessayer plus tard.");
        });
    }

    $scope.vdmGetImage = function(post) {
        if (post.media_url)
            return "<a href=\"http://vdm.3ie.fr" + post.media_url + "\">"
                    + "<img class=\"vdm-image\" src=\"http://vdm.3ie.fr" + post.media_url + "\">"
                    + "</a>";
    }
});


app.controller("loginctrl", function($scope, $http, $window, $document) {
    $scope.login = function(user) {
        var connectedUser = getUser();
        if (connectedUser)
            toastr.info('Vous êtes déjà connecté !');

        else if (user && 'username' in user && 'password' in user) {
            var request = $http({
                method: "POST",
                url: "http://vdm.3ie.fr/user/login",
                data: {
                    username: user.username,
                    password: user.password
                }
            });
            
            request.success(function(data) {
                toastr.success("Bonjour " + data.username + " !<br />Vous êtes désormais connecté.");
                userInfos = {
                    id: data.id,
                    username: data.username,
                    token: data.token
                }
                $window.sessionStorage.setItem(user.username, JSON.stringify(userInfos));
                
                window.setTimeout(function() {
                    window.location.replace("index.html");
                }, 1500);
            });

            request.error(function(data) {
                if (data.code == 401) {
                    toastr.error("Informations incorrectes.");
                    $("#password").val('');
                }
                else if (data.code == 400)
                    toastr.warning("Veuillez remplir le formulaire.");
                else
                    toastr.error("Une erreur de connexion est survenue.\nVeuillez réessayer plus tard.");
            });
        }

        else
            toastr.warning("Veuillez remplir le formulaire de connexion.");

    }

    $scope.register = function(user) {
        var connectedUser = getUser();
        if (connectedUser)
            toastr.info('Vous êtes déjà connecté.<br/>Pourquoi vous réinscrire ?');

        else if (user && 'username' in user && 'password' in user) {
            var request = $http({
                method: "POST",
                url: "http://vdm.3ie.fr/user/",
                data: {
                    username: user.username,
                    password: user.password
                }
            });
            
            request.success(function(data) {
                toastr.success("Bienvenu parmi nous " + data.username + " !<br/>Vous êtes désormais connecté.");
                userInfos = {
                    id: data.id,
                    username: data.username,
                    token: data.token
                }
                $window.sessionStorage.setItem(user.username, JSON.stringify(userInfos));
                
                window.setTimeout(function() {
                    window.location.replace("index.html");
                }, 1500);
            });

            request.error(function(data) {
                if (data.code == 400)
                    toastr.warning("Veuillez remplir le formulaire.");
                else if (data.code == 401)
                    toastr.error("Informations incorrectes.");
                else if (data.code == 409)
                    toastr.error("Ce pseudo est déjà pris.<br />Veuillez en choisir un autre.");
                else if (data.code == 422)
                    toastr.warning("Votre pseudo est trop court<br/>(3 caractères minimum).");
                else
                    toastr.error("Une erreur de connexion est survenue.\nVeuillez réessayer plus tard.");
            });
        }

        else
            toastr.warning("Veuillez remplir le formulaire d'inscription.");
    }
});


app.controller("newStatusCtrl", function($rootScope, $scope, $http) {

    var user = getUser();
    var statusInfos = {};


    function getLocation(callback) {
        var checkLocation = ($("input[name=checkLocation]").is(':checked'));
        if (!checkLocation) {
            callback();
            return;
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    toastr.success("Votre position sera liée au status.");
                    callback(
                        [position.coords.latitude,
                         position.coords.longitude]
                    );
                },
                function() {
                    toastr.warning("Géolocalisation désactivée.<br />");
                    callback();
                },
                {enableHighAccuracy:false, maximumAge:60000, timeout:27000}
            );
        }
        else {
            toastr.warning('Votre navigateur ne prend pas en charge la géolocalisation.');
            callback();
        }
    }

    $scope.newStatus = function() {
        if (!$scope.vdmStatus)
            toastr.warning("Votre VDM semble un peu vide...");

        else {
            statusInfos.status = $scope.vdmStatus;
            if (document.getElementById('file').files[0])
                statusInfos.media = document.getElementById('file').files[0];


            getLocation(function(currentPosition)
            {
                if (currentPosition) {
                    statusInfos.latitude = currentPosition[0];
                    statusInfos.longitude = currentPosition[1];
                }

                console.log(statusInfos);
                var request = $http({
                    method: "POST",
                    url: "http://vdm.3ie.fr/status/",
                    headers: {
                        "Authorization": user.token
                    },
                    data: statusInfos,
                    transformRequest: function (data, headersGetter) {
                        var formData = new FormData();
                        angular.forEach(data, function (value, key) {
                            formData.append(key, value);
                        });

                        var headers = headersGetter();
                        delete headers['Content-Type'];

                        return formData;
                    }
                });
                
                request.success(function(data) {
                    toastr.success("Votre VDM a bien été publiée !");
                    $rootScope.$broadcast('updatePosts');
                    $("body").animate({scrollTop : 0},800);
                    $("#vdmStatus").val('');
                });

                request.error(function(data) {
                    if (data.code == 400)
                        toastr.warning("Veuillez remplir le formulaire.");
                    else if (data.code == 401)
                        toastr.error("Informations incorrectes.");
                    else if (data.code == 422)
                        toastr.warning("Votre VDM doit contenir au minimum 10 caractères.");
                    else
                        toastr.error("Une erreur de connexion est survenue.\nVeuillez réessayer plus tard.");
                });
            });

        }
    }
});

function Ctrl($rootScope, $http) {
    $rootScope.model = {
        name: "",
        comments: ""
    };

    $rootScope.file = {};

    $rootScope.$on("fileSelected", function (event, args) {
        $rootScope.$apply(function () {            
            $rootScope.file = args.file;
        });
    });
    
};